FROM golang:latest AS compiler
WORKDIR /scr
COPY hello_golang.go .
RUN go build -ldflags "-s -w" hello_golang.go && \
    strip hello_golang

FROM scratch
COPY --from=compiler /scr/hello_golang .
CMD ["./hello_golang"]