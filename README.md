# Projeto: Desafio 1 do módulo Docker  do Curso FullCycle 3.0 
## Curso: FullCycle 3.0
## Módulo: Docker  
### Desafio 1  
  
GitLab: https://gitlab.com/fdmneto/fullcycle_docker_d1  
Docker Hub: https://hub.docker.com/r/fdmneto/fullcycle_docker_d1

Arquivos:  
    - hello_golang.go: Programa em Go que imprime na tela a frase "Full Cycle Rocks!!"  
    - Dockerfile: Dockerfile para a geração do container com menos de 2M bytes que ao ser executado imprime na tela a frase "FullCycle Rocks!"  

Executar diretamento do Docker Hub: docker run fdmneto/fullcycle_docker_d1  





